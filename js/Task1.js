"use strict";

/* Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
4. Яка різниця між nodeList та HTMLCollection?

Практичні завдання
1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
Використайте 2 способи для пошуку елементів.
Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).

 */

console.log("1. Кожен таг HTML є обʼєктом, DOM представляє собою структуру цих обʼєктів у вигляді дерева, яким розробник може керувати та змінювати його");
console.log("2. innerText повертає тільки текст, а innerHTMLповретає текст з тегами");
console.log("3. Можна звернутися за класом, id, за тагом, але краще за все querySelector(name)");
console.log("4. NodeList та HTMLCollection є двома різними типами об'єктів, NodeList - отримується в результаті виклику методів, HTMLCollection - отримується, наприклад, при виклику document.getElementsByTagName() або властивостей, як children у багатьох DOM-елементів, HTMLCollection може бути тільки живим, автоматично оновлюється з сайтом та має мало методів для роботи з ним. NodeList може бути і статичним");

console.log("PRACTICE");

console.log("Task 1");

const featuresList = document.getElementsByClassName("feature");

console.log(featuresList);

const featuredList2 = document.querySelectorAll(".feature");

console.log(featuredList2);

// document.getElementsByClassName("feature").style.textAlign = "center";
featuresList.style.textAlign = "left";

console.log(featuresList);